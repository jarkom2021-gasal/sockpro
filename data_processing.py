import csv
import re

file_csv = open("data.csv")
data = csv.reader(file_csv)

header = next(data)

new_header = list(header).copy()
new_header.extend(["Nilai Akhir", "Grade"])
result = " ".join(new_header) + "\n"

to_int_func = lambda value: int(re.split('|'.join(map(re.escape, ["(", "%", ")"])), value)[1])/100
bobot_component = list(map(to_int_func, header[2:]))

for row in data:
    grade_value = 0
    grade_alphabet = ""
    i = 0
    
    for nilai in row[2:]:
        grade_value += int(nilai)*bobot_component[i]
        i+=1

    if grade_value >= 85:
        grade_alphabet = "A"
    elif grade_value >= 80 and grade_value < 85:
        grade_alphabet = "A-"
    elif  grade_value >= 75 and grade_value < 80:
        grade_alphabet = "B+"
    elif  grade_value >= 70 and grade_value < 75:
        grade_alphabet = "B"
    elif  grade_value >= 65 and grade_value < 70:
        grade_alphabet = "B-"
    elif  grade_value >= 60 and grade_value < 65:
        grade_alphabet = "C+"
    elif  grade_value >= 55 and grade_value < 60:
        grade_alphabet = "C"
    elif  grade_value >= 40 and grade_value < 55:
        grade_alphabet = "D"
    else:
        grade_alphabet = "E"

    grade_value = "{:.2f}".format(grade_value)
    graded_row = row.copy()
    graded_row.extend([grade_value, grade_alphabet])

    string_graded_row = " ".join(graded_row) + "\n"
    result = result + string_graded_row

print(result)



    

