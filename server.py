#!/usr/bin/env python3

import csv
import enum
import re
import io
import random
import socket
import threading
import datetime

welcome_prompt = """\
                _         _____               _       _____      _      
     /\        | |       / ____|             | |     / ____|    | |     
    /  \  _   _| |_ ___ | |  __ _ __ __ _  __| | ___| |     __ _| | ___ 
   / /\ \| | | | __/ _ \| | |_ | '__/ _` |/ _` |/ _ \ |    / _` | |/ __|
  / ____ \ |_| | || (_) | |__| | | | (_| | (_| |  __/ |___| (_| | | (__ 
 /_/    \_\__,_|\__\___/ \_____|_|  \__,_|\__,_|\___|\_____\__,_|_|\___|

An automatic final grade calculator for courses in Fasilkom UI
Author: Abdurrafi Arief, M. Rifqy Zulkarnaen, Rafi M. Daffa (Jarkom-B)                                                                        

To terminate the program, send the Ctrl+C signal.                                                                        
"""

hostname = ""
port = 54321
buffer_size = 1024

active_threads = []
active_threads_lock = threading.RLock()

client_activities = {}
client_activities_lock = threading.RLock()

class WorkStatus(enum.Enum):
    STARTED = 0
    PROCESSING = 1
    DONE = 2

class WorkType(enum.Enum):
    LIST_WORKS = 0
    GET_TEMPLATE = 1
    PROCESS_CSV = 2


class ClientHandler(threading.Thread):
    def __init__(self, connection:socket, address, group=None, target=None, name=None):
        super().__init__(group=group, target=target, name=name)
        self.id = random.randint(10000, 9999999)
        self.connection: socket = connection
        self.address = address[0]
        self.status = WorkStatus.STARTED
        self.worktype = 0

    def get_work_list(self):
        result = "| {:7} | {:12} | {:10} |\n".format("ID", "Work Type", "Status")
        client_activities_lock.acquire()
        work_list = client_activities.get(self.address, [])
        client_activities_lock.release()
        for work in work_list:
            result += "| {:7} | {:12} | {:10} |".format(work.id, str(WorkType(work.worktype).name), str(WorkStatus(work.status).name))
            result += "\n"
        return result

    
    def create_template(self):
        headers = ["No", "NPM"] # Fixed headers
        for i in range(1, 11): # Component headers
            headers.append(f"Komponen{i} (x%)")
        
        template = ",".join(headers)
        for i in range(50):
            template+=f"\n{i},,,,,,,,,,,"

        return template

    def receive_data(self):
        csv_data = b''
        receive_buffer = b'0'*buffer_size
        while len(receive_buffer) == buffer_size:
            receive_buffer = self.connection.recv(buffer_size)
            csv_data += receive_buffer
        return io.TextIOWrapper(io.BytesIO(csv_data))

    def handle_csv(self, stream):
        reader = csv.reader(stream)
        header = next(stream)

        # Creating new CSV File for Result
        filename = "graded_data.csv"
        header = header.replace("\n", "")
        
        new_header = header + ", Nilai Akhir, Grade \n"
        result = new_header
        header = header.split(",")
        # Take the component value
        to_int_func = lambda value: int(re.split('|'.join(map(re.escape, ["(", "%", ")"])), value)[1])/100
        bobot_component = list(map(to_int_func, header[2:]))
        
        # Calculate student grade
        for row in stream:
            row = row.split(",")
            grade_value = 0
            grade_alphabet = ""
            i = 0
            for nilai in row[2:]:
                grade_value += int(nilai)*bobot_component[i]
                i+=1
                
                
            # Checking Alphabet Grade
            if grade_value >= 85:
                grade_alphabet = "A"
            elif grade_value >= 80 and grade_value < 85:
                grade_alphabet = "A-"
            elif  grade_value >= 75 and grade_value < 80:
                grade_alphabet = "B+"
            elif  grade_value >= 70 and grade_value < 75:
                grade_alphabet = "B"
            elif  grade_value >= 65 and grade_value < 70:
                grade_alphabet = "B-"
            elif  grade_value >= 60 and grade_value < 65:
                grade_alphabet = "C+"
            elif  grade_value >= 55 and grade_value < 60:
                grade_alphabet = "C"
            elif  grade_value >= 40 and grade_value < 55:
                grade_alphabet = "D"
            else:
                grade_alphabet = "E"
            
            grade = "{:.2f}".format(grade_value)
            graded_row = row.copy()
            graded_row.extend([grade, grade_alphabet])
            graded_row[4] = graded_row[4].replace("\n", "")
            string_graded_row = ",".join(graded_row) + "\n"
            print(string_graded_row)
            result = result + string_graded_row
        
        return result           

    def run(self):
        print(f"[{datetime.datetime.now()} CONNECTION][{self.id}] Received connection from {self.address}")
        self.connection.sendall(
            f"""You are connected to AutoGradeCalc Node ({get_public_ip_address()}).
This Job's ID is {self.id}.
Input 0 to see your job list, 1 to download CSV template, or 2 to upload CSV template""".encode())
        
        self.worktype = int(self.connection.recv(1024).decode())
        self.status = WorkStatus.PROCESSING
        if self.worktype == 0:
            register_work(self, self.address)
            self.connection.sendall(self.get_work_list().encode())
        if self.worktype == 1:
            register_work(self, self.address)
            csv_template = self.create_template()
            self.connection.sendall(csv_template.encode())
        elif self.worktype == 2:
            register_work(self, self.address)
            data_stream = self.receive_data()
            result = self.handle_csv(data_stream)
            self.connection.sendall(result.encode())

        self.status = WorkStatus.DONE
        print(f"[{datetime.datetime.now()} CONNECTION][{self.id}] Finished handling request for {self.address}")
        self.connection.close()
        unregister_thread(self)

def register_work(thread, address):
    client_activities_lock.acquire()
    client_activities.setdefault(address, []).append(thread)
    client_activities_lock.release()

def register_thread(thread):
    active_threads_lock.acquire()
    active_threads.append(thread)
    active_threads_lock.release()

def unregister_thread(thread):
    active_threads_lock.acquire()
    active_threads.remove(thread)
    active_threads_lock.release()

def get_public_ip_address():
    import urllib.request
    ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')
    return ip

def threadsample(conn, address, count):
    lv = threading.local()
    lv.recv_text = "A"*buffer_size
    while True:
        lv.recv_text = conn.recv(buffer_size)
        print(f"{count} - lv.recv_text")

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sc:
    sc.bind((hostname, port))
    sc.listen(5)
    print(welcome_prompt)
    print(f"[{datetime.datetime.now()} SYSTEM] Using IP {get_public_ip_address()}")
    print(f"[{datetime.datetime.now()} SYSTEM] Server now listening through port {port}")
    count = 1
    try:
        while True:
            conn, address = sc.accept()
            t = ClientHandler(conn, address)
            register_thread(t)
            t.start()
            count+=1
            
    except (KeyboardInterrupt, SystemExit):
        print(f"[{datetime.datetime.now()} SYSTEM] Exit signal received, waiting for jobs to complete...")
        for thread in active_threads.copy():
            thread.join()
        print(f"[{datetime.datetime.now()} SYSTEM] Exiting program...")