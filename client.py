import csv
import io
import progressbar
import random
import socket
import threading

HOST = ['127.0.0.1', '52.55.36.207', '50.17.68.16', '34.197.81.79'] # The server's hostname or IP address (can be multiple to try multiple servers)
PORT = 54321 # The port used by the server


def make_csv(filename, data):
    rows = data.split("\n")
    with open(filename, "x") as csvfile:
        csvwriter = csv.writer(csvfile, lineterminator = '\n')
        for row in rows:
            fields = row.split(",")
            csvwriter.writerow(fields)
        csvfile.close()

    return "Downloaded csv file"

def connection_handler(s, host, port):
    s.connect((host, port))
    data = s.recv(1024)
    print(data.decode())
    number = input()
    while(number not in ("0", "1","2")):
        print("Please enter 1 or 2")
        number = input()
    s.send(bytes(number, "UTF-8"))
    if(number == "0"):
        data = s.recv(1024)
        print(str(data.decode()))
        s.close()
    elif(number == "1"):
        data = s.recv(1024)
        print(make_csv("Template.csv", str(data.decode())))
        s.close()
    elif(number == "2"):
        print("Please enter the filename with it's absolute location")
        filelocation = input()
        f = open(filelocation, "rb")
        l = f.read(1024)
        while (l):
            s.send(l)
            l = f.read(1024)
        data = s.recv(4096)
        print(make_csv("Nilai.csv", str(data.decode())))
        s.close()
    elif(number == "0"):
        data = s.recv(4096)
        print(str(data.decode()))
        s.close()
    
welcome_prompt = """\
                _         _____               _       _____      _      
     /\        | |       / ____|             | |     / ____|    | |     
    /  \  _   _| |_ ___ | |  __ _ __ __ _  __| | ___| |     __ _| | ___ 
   / /\ \| | | | __/ _ \| | |_ | '__/ _` |/ _` |/ _ \ |    / _` | |/ __|
  / ____ \ |_| | || (_) | |__| | | | (_| | (_| |  __/ |___| (_| | | (__ 
 /_/    \_\__,_|\__\___/ \_____|_|  \__,_|\__,_|\___|\_____\__,_|_|\___|

An automatic final grade calculator for courses in Fasilkom UI
Author: Abdurrafi Arief, M. Rifqy Zulkarnaen, Rafi M. Daffa (Jarkom-B)                                                                        

Welcome to the client program!
Please wait while we check the status of our nodes"""
print(welcome_prompt)
node_status = "| {:17} | {:12} |\n".format("IP", "Status")

used_node = ""

widgets = [
         'Progress: ', progressbar.Percentage(),
         ' ', progressbar.Bar(marker='=', left='[', right=']')
     ]
bar = progressbar.ProgressBar(widgets=widgets, max_value=len(HOST)-1)
bar.start()

for index, host in enumerate(HOST):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.settimeout(3.0)
        try:
            s.connect((host, PORT))
            if used_node == "":
                node_status += "| {:17} | {:12} |\n".format(host, "Active*")
                used_node = host
            else:
                node_status += "| {:17} | {:12} |\n".format(host, "Active")
        except (OSError, ConnectionError):
            node_status += "| {:17} | {:12} |\n".format(host, "Inactive")
    bar.update(index)

print("\n")
print("Node Status (* = node will be used)")
print(node_status)

if used_node != "":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        connection_handler(s, used_node, PORT)
else:
    print("No node is available right now.")





